# FROM nginx:1.10.3-alpine
FROM nginx:alpine

COPY /html/index.html /usr/share/nginx/html
COPY /config/nginx.conf /etc/nginx/nginx.conf

EXPOSE 80 12345